# ListVESA

ListVESA is a utility to report which VESA video modes are supported by the system's hardware. You can use command line options to tailor information listed to one specific mode, modes supporting a specific color bit depth, modes supporting linear frame buffer, or simply general information on the video adapter itself or a table summarizing available modes with no detailed screen data.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## LISTVESA.LSM

<table>
<tr><td>title</td><td>ListVESA</td></tr>
<tr><td>version</td><td>1.11</td></tr>
<tr><td>entered&nbsp;date</td><td>2023-05-02</td></tr>
<tr><td>description</td><td>ListVESA is a utility to report which VESA video modes are supported by the system's hardware. You can use command line options to tailor information listed to one specific mode, modes supporting a specific color bit depth, modes supporting linear frame buffer, or simply general information on the video adapter itself or a table summarizing available modes with no detailed screen data.</td></tr>
<tr><td>keywords</td><td>list, mode, VESA, video</td></tr>
<tr><td>author</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://www.mercurycoding.com/downloads.html#DOS</td></tr>
<tr><td>original&nbsp;site</td><td>https://www.mercurycoding.com/downloads.html#DOS</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GPLv2, with source code, no warranty](LICENSE)</td></tr>
</table>
